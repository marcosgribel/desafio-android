package desafio.cs.com.br.desafio_android_cs;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import desafio.cs.com.br.desafio_android_cs.business.interactor.GitHubInteractor;
import desafio.cs.com.br.desafio_android_cs.business.interactor.impl.GitHubInteractorImpl;
import desafio.cs.com.br.desafio_android_cs.model.entity.Pull;
import desafio.cs.com.br.desafio_android_cs.model.entity.SchemaRepository;
import rx.Observable;
import rx.observers.TestSubscriber;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */

@RunWith(JUnit4.class)
public class GitHubInteractorUnitTest {

    GitHubInteractor interactor;

    @Before
    public void before(){
        interactor = new GitHubInteractorImpl();
    }

    @Test
    public void test_repositorios_obterTodos_sucesso(){
        Observable<SchemaRepository> observable = interactor.obterRepositorios("starts", 1);

        TestSubscriber<SchemaRepository> subscriber = new TestSubscriber<>();
        observable.subscribe(subscriber);
        subscriber.assertCompleted();
        subscriber.assertNoErrors();

    }

    @Test
    public void test_pulls_obterTotos_sucesso(){
        final String USUARIO = "ReactiveX";
        final String REPOSITORIO = "RxJava";

        Observable<List<Pull>> observable = interactor.obterPulls(USUARIO, REPOSITORIO);

        TestSubscriber<List<Pull>> subscriber = new TestSubscriber<>();
        observable.subscribe(subscriber);
        subscriber.assertCompleted();
        subscriber.assertNoErrors();

    }


}
