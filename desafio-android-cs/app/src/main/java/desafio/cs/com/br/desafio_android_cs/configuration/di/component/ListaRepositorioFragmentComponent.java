package desafio.cs.com.br.desafio_android_cs.configuration.di.component;

import dagger.Subcomponent;
import desafio.cs.com.br.desafio_android_cs.configuration.PerActivity;
import desafio.cs.com.br.desafio_android_cs.configuration.di.module.ListaPullFragmentModule;
import desafio.cs.com.br.desafio_android_cs.configuration.di.module.ListaRepositorioFragmentModule;
import desafio.cs.com.br.desafio_android_cs.presentation.view.ListaPull;
import desafio.cs.com.br.desafio_android_cs.ui.fragment.ListaPullFragment;
import desafio.cs.com.br.desafio_android_cs.ui.fragment.ListaRepositorioFragment;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 *
 * All Rights Reserved.
 */

@PerActivity
@Subcomponent(
        modules = {
                ListaRepositorioFragmentModule.class
        }
)
public interface ListaRepositorioFragmentComponent {

        void inject(ListaRepositorioFragment fragment);

}
