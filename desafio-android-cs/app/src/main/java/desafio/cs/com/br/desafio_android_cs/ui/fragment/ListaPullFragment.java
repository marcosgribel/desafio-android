package desafio.cs.com.br.desafio_android_cs.ui.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import desafio.cs.com.br.desafio_android_cs.R;
import desafio.cs.com.br.desafio_android_cs.configuration.App;
import desafio.cs.com.br.desafio_android_cs.configuration.di.module.ListaPullFragmentModule;
import desafio.cs.com.br.desafio_android_cs.configuration.di.module.ListaRepositorioFragmentModule;
import desafio.cs.com.br.desafio_android_cs.model.entity.Base;
import desafio.cs.com.br.desafio_android_cs.model.entity.Pull;
import desafio.cs.com.br.desafio_android_cs.presentation.adapter.PullArrayAdapter;
import desafio.cs.com.br.desafio_android_cs.presentation.presenter.ListaPullFragmentPresenter;
import desafio.cs.com.br.desafio_android_cs.presentation.view.ListaPull;

import static android.text.Html.FROM_HTML_MODE_LEGACY;

/**
 *
 */
public class ListaPullFragment extends BaseFragment implements ListaPull.Fragment.View {


    @Inject
    ListaPullFragmentPresenter presenter;

    @BindView(R.id.txt_quantidade_pulls)
    TextView txtQtdPulls;
    @BindView(R.id.list_view_pulls)
    ListView mListView;
    @BindView(R.id.img_nenhum_item)
    ImageView mImgNenhumItem;
    @BindView(R.id.txt_nenhum_item)
    TextView mTxtNenhumItem;

    private PullArrayAdapter mAdapter;


    private final ListView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Pull pull = mAdapter.getItem(position);

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(pull.getHtmlUrl()));
            startActivity(intent);
        }
    };


    public ListaPullFragment() {
        // Required empty public constructor
    }

    public static ListaPullFragment newInstance(String usuario, String repositorio, int qtdErro, int qtdErroFechado){
        ListaPullFragment fragment = new ListaPullFragment();
        Bundle args = new Bundle();
        args.putString("nome_usuario", usuario);
        args.putString("nome_repositorio", repositorio);
        args.putInt("qtd_pull", qtdErro);
        args.putInt("qtd_pull_fechado", qtdErroFechado);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Dagger dependency injection configuration
        App.get(this.getContext())
                .getAppComponent()
                .plus(new ListaPullFragmentModule(this))
                .inject(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(getString(R.string.itens), (ArrayList<? extends Parcelable>) mAdapter.getItens());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lista_pull, container, false);

        ButterKnife.bind(this, view);

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
        getActivity().setTitle(args.getString(getString(R.string.nome_repositorio)));

        txtQtdPulls.setText(
                Html.fromHtml(
                String.format(getString(R.string.quantida_pulls_aberto_e_fechado), args.getInt("qtd_pull"), args.getInt("qtd_pull_fechado")))
        );

        if( savedInstanceState == null ){

            mAdapter = new PullArrayAdapter(getContext(), R.layout.item_list_pulls);
            presenter.carregar(args.getString(getString(R.string.nome_usuario)), args.getString(getString(R.string.nome_repositorio)));
        } else {
            List<Pull> pulls = savedInstanceState.getParcelableArrayList(getString(R.string.itens));
            mAdapter = new PullArrayAdapter(getContext(), R.layout.item_list_pulls, pulls);
        }

        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(mOnItemClickListener);


    }

    @Override
    public void addItem(List<Pull> itens) {
        if(mAdapter != null){
            mAdapter.addAll(itens);
        }
    }


    @Override
    public void showListaVazia(boolean value) {
        if(value){
            mListView.setVisibility(View.VISIBLE);
            mImgNenhumItem.setVisibility(View.VISIBLE);
            mTxtNenhumItem.setVisibility(View.VISIBLE);
        } else {
            mListView.setVisibility(View.VISIBLE);
            mImgNenhumItem.setVisibility(View.GONE);
            mTxtNenhumItem.setVisibility(View.GONE);
        }

    }
}
