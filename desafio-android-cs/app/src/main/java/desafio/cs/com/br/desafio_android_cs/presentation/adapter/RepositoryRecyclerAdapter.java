package desafio.cs.com.br.desafio_android_cs.presentation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import desafio.cs.com.br.desafio_android_cs.R;
import desafio.cs.com.br.desafio_android_cs.model.entity.Item;
import desafio.cs.com.br.desafio_android_cs.ui.OnRecyclerItemClickListener;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */

public class RepositoryRecyclerAdapter extends RecyclerView.Adapter<RepositoryRecyclerAdapter.RepositoryRecyclerViewHolder> {

    public OnRecyclerItemClickListener clickListener;

    private Context context;
    private List<Item> itens = new ArrayList<>();

    public RepositoryRecyclerAdapter(Context context) {
            this.context = context;
    }

    public RepositoryRecyclerAdapter(Context context, List<Item> itens) {
        this.context = context;
        this.itens = itens;
    }

    public void addAll(List<Item> moreItens){
        itens.addAll(moreItens);
        notifyDataSetChanged();
    }

    public Item getItem(int position){
        return itens.get(position);
    }

    @Override
    public RepositoryRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_repositories, parent, false);
        return new RepositoryRecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RepositoryRecyclerViewHolder holder, int position) {
        Item item = itens.get(position);

        holder.txtNomeRepositorio.setText(item.getFullName());
        holder.txtDescricaoRepositorio.setText(item.getDescription());

        holder.txtNomeUsuario.setText(item.getOwner().getLogin());
        holder.txtNomeUsuarioCompleto.setText(item.getOwner().getLogin());

        Glide.with(context)
                .load(item.getOwner().getAvatarUrl())
                .override(50,50)
                .crossFade()
                .into(holder.imgAvatarUsuario);

        holder.txtQtdForks.setText(item.getForksCount().toString());
        holder.txtQtdStars.setText(item.getStargazersCount().toString());
    }

    @Override
    public int getItemCount() {
        return itens.size();
    }

    public List<Item> getItens(){
        return itens;
    }

    public void setClickListener(OnRecyclerItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    /**
     * View Holder class
     */
    public class RepositoryRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.txt_nome_repositorio)
        TextView txtNomeRepositorio;
        @BindView(R.id.txt_descricao_repositorio)
        TextView txtDescricaoRepositorio;

        @BindView(R.id.img_avatar_usuario)
        ImageView imgAvatarUsuario;
        @BindView(R.id.txt_nome_usuario)
        TextView txtNomeUsuario;
        @BindView(R.id.txt_nome_usuario_completo)
        TextView txtNomeUsuarioCompleto;
        @BindView(R.id.txt_qtd_forks)
        TextView txtQtdForks;
        @BindView(R.id.txt_qtd_stars)
        TextView txtQtdStars;


        public RepositoryRecyclerViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onClick(v, getAdapterPosition());
        }
    }
}
