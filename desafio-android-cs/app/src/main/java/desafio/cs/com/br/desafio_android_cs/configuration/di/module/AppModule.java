package desafio.cs.com.br.desafio_android_cs.configuration.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import desafio.cs.com.br.desafio_android_cs.configuration.App;

/**
 * Created by marcosgribel on 10/6/16.
 */

@Module
public class AppModule {

    App app;

    public AppModule(App app) {
        this.app = app;
    }


    @Provides
    @Singleton
    public App provideApplication(){
        return app;
    }


}
