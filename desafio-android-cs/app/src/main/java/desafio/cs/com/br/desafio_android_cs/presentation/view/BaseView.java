package desafio.cs.com.br.desafio_android_cs.presentation.view;

import java.util.List;

import desafio.cs.com.br.desafio_android_cs.model.entity.Item;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */

public interface BaseView {


    void showErro(String mensagem);

    void showProgress(boolean isShow);

}
