package desafio.cs.com.br.desafio_android_cs.configuration;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {

}
