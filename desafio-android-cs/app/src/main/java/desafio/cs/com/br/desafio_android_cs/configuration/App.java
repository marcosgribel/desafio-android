package desafio.cs.com.br.desafio_android_cs.configuration;

import android.app.Application;
import android.content.Context;

import desafio.cs.com.br.desafio_android_cs.configuration.di.component.AppComponent;
import desafio.cs.com.br.desafio_android_cs.configuration.di.component.DaggerAppComponent;
import desafio.cs.com.br.desafio_android_cs.configuration.di.module.AppModule;


/**
 * Created by marcosgribel on 10/6/16.
 */

public class App extends Application {

    private AppComponent appComponent;


    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    /**
     *
     * @return appComponent
     */
    public AppComponent getAppComponent() {
        return appComponent;
    }

}
