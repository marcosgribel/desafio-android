package desafio.cs.com.br.desafio_android_cs.configuration.di.component;

import dagger.Subcomponent;
import desafio.cs.com.br.desafio_android_cs.configuration.PerActivity;
import desafio.cs.com.br.desafio_android_cs.configuration.di.module.ListaPullFragmentModule;
import desafio.cs.com.br.desafio_android_cs.configuration.di.module.ListaRepositorioFragmentModule;
import desafio.cs.com.br.desafio_android_cs.ui.fragment.ListaPullFragment;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */

@PerActivity
@Subcomponent(
        modules = {
                ListaPullFragmentModule.class
        }
)
public interface ListaPullFragmentComponent {

    void inject(ListaPullFragment fragment);

}
