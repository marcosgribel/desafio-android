package desafio.cs.com.br.desafio_android_cs.presentation.presenter;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */

public interface ListaRepositorioFragmentPresenter {

    void carregar(int pagina);
}
