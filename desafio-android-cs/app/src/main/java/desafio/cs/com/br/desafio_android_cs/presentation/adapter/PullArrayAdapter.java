package desafio.cs.com.br.desafio_android_cs.presentation.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import desafio.cs.com.br.desafio_android_cs.R;
import desafio.cs.com.br.desafio_android_cs.model.entity.Pull;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */

public class PullArrayAdapter extends ArrayAdapter<Pull> {

    private List<Pull> itens = new ArrayList<>();

    @BindView(R.id.txt_titulo)
    TextView txtTitulo;
    @BindView(R.id.txt_descricao)
    TextView txtDescricao;

    @BindView(R.id.img_avatar_usuario)
    ImageView imgAvatarUsuario;

    @BindView(R.id.txt_nome_usuario)
    TextView txtNomeUsuario;
    @BindView(R.id.txt_nome_usuario_completo)
    TextView txtNomeUsuarioCompleto;

    public PullArrayAdapter(Context context, int resource) {
        super(context, resource);
    }

    public PullArrayAdapter(Context context, int resource, List<Pull> pulls) {
        super(context, resource, pulls);
        this.itens = pulls;
    }

    @Nullable
    @Override
    public Pull getItem(int position) {
        return itens.get(position);
    }

    @Override
    public int getCount() {
        return itens.size();
    }

    public List<Pull> getItens(){
        return this.itens;
    }

    public void addAll(List<Pull> pulls){
        itens.addAll(pulls);
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Pull item = itens.get(position);

        if( convertView == null ){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_list_pulls, parent, false);
        }

        ButterKnife.bind(this, convertView);

        txtTitulo.setText(item.getTitle());
        txtDescricao.setText(item.getBody());

        Glide.with(getContext())
                .load(item.getUser().getAvatarUrl())
                .override(50,50)
                .crossFade()
                .into(imgAvatarUsuario);

        txtNomeUsuario.setText(item.getUser().getLogin());
        txtNomeUsuarioCompleto.setText(item.getUser().getLogin());

        return convertView;
    }
}
