package desafio.cs.com.br.desafio_android_cs.presentation.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import desafio.cs.com.br.desafio_android_cs.R;
import desafio.cs.com.br.desafio_android_cs.model.entity.Item;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */

public class RepositorioArrayAdapter extends ArrayAdapter<Item> {

    @BindView(R.id.txt_nome_repositorio)
    TextView txtNomeRepositorio;
    @BindView(R.id.txt_descricao_repositorio)
    TextView txtDescricaoRepositorio;

    @BindView(R.id.img_avatar_usuario)
    ImageView imgAvatarUsuario;
    @BindView(R.id.txt_nome_usuario)
    TextView txtNomeUsuario;
    @BindView(R.id.txt_nome_usuario_completo)
    TextView txtNomeUsuarioCompleto;
    @BindView(R.id.txt_qtd_forks)
    TextView txtQtdForks;
    @BindView(R.id.txt_qtd_stars)
    TextView txtQtdStars;

    private List<Item> itens = new ArrayList<>();

    public RepositorioArrayAdapter(Context context, int resource) {
        super(context, resource);
    }

    public RepositorioArrayAdapter(Context context, int resource, List<Item> itens) {
        super(context, resource, itens);
        this.itens = itens;
    }


    @Override
    public int getCount() {
        return itens.size();
    }

    public List<Item> getItens(){
        return this.itens;
    }

    @Nullable
    @Override
    public Item getItem(int position) {
        return itens.get(position);
    }

    public void addAll(List<Item> itens){
        this.itens.addAll(itens);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Item item = itens.get(position);

        if( convertView == null ){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_list_repositories, parent, false);
        }
        ButterKnife.bind(this, convertView);

        txtNomeRepositorio.setText(item.getFullName());
        txtDescricaoRepositorio.setText(item.getDescription());

        txtNomeUsuario.setText(item.getOwner().getLogin());
        txtNomeUsuarioCompleto.setText(item.getOwner().getLogin());

        Glide.with(getContext())
                .load(item.getOwner().getAvatarUrl())
                .override(50,50)
                .crossFade()
                .into(imgAvatarUsuario);

        txtQtdForks.setText(item.getForksCount().toString());
        txtQtdStars.setText(item.getStargazersCount().toString());

        return convertView;
    }
}
