package desafio.cs.com.br.desafio_android_cs.presentation.view;

import java.util.List;

import desafio.cs.com.br.desafio_android_cs.model.entity.Pull;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */

public interface ListaPull {

    interface View extends BaseView {

    }

    interface Fragment {

        interface View extends ListaPull.View {

            void addItem(List<Pull> itens);

            void showListaVazia(boolean value);
        }

    }
}
