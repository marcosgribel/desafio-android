package desafio.cs.com.br.desafio_android_cs.configuration.di.component;


import javax.inject.Singleton;

import dagger.Component;
import desafio.cs.com.br.desafio_android_cs.configuration.di.module.AppModule;
import desafio.cs.com.br.desafio_android_cs.configuration.di.module.ListaPullFragmentModule;
import desafio.cs.com.br.desafio_android_cs.configuration.di.module.ListaRepositorioFragmentModule;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 *
 * All Rights Reserved.
 */

@Singleton
@Component(
        modules = {
            AppModule.class
        }
)
public interface AppComponent {

        ListaRepositorioFragmentComponent plus(ListaRepositorioFragmentModule module);

        ListaPullFragmentComponent plus(ListaPullFragmentModule module);

}
