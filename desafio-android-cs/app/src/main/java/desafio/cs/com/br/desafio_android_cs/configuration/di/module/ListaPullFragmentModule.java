package desafio.cs.com.br.desafio_android_cs.configuration.di.module;

import dagger.Module;
import dagger.Provides;
import desafio.cs.com.br.desafio_android_cs.configuration.PerActivity;
import desafio.cs.com.br.desafio_android_cs.presentation.presenter.ListaPullFragmentPresenter;
import desafio.cs.com.br.desafio_android_cs.presentation.presenter.impl.ListaPullFragmentPresenterImpl;
import desafio.cs.com.br.desafio_android_cs.presentation.view.ListaPull;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */

@Module(
        includes = InteractorModule.class
)
public class ListaPullFragmentModule {

    public final ListaPull.Fragment.View mView;

    public ListaPullFragmentModule(ListaPull.Fragment.View mView) {
        this.mView = mView;
    }

    @PerActivity
    @Provides
    ListaPull.Fragment.View provideListaPullFragmentView(){
        return this.mView;
    }

    @PerActivity
    @Provides
    ListaPullFragmentPresenter provideListaRepositorioFragmentPresenter(ListaPullFragmentPresenterImpl presenter){
        return presenter;
    }


}
