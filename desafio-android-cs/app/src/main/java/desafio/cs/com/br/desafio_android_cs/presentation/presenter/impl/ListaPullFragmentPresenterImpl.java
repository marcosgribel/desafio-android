package desafio.cs.com.br.desafio_android_cs.presentation.presenter.impl;

import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import desafio.cs.com.br.desafio_android_cs.business.interactor.GitHubInteractor;
import desafio.cs.com.br.desafio_android_cs.model.entity.Pull;
import desafio.cs.com.br.desafio_android_cs.presentation.presenter.ListaPullFragmentPresenter;
import desafio.cs.com.br.desafio_android_cs.presentation.view.ListaPull;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */

public class ListaPullFragmentPresenterImpl implements ListaPullFragmentPresenter {
    private static final String TAG = ListaPullFragmentPresenterImpl.class.getSimpleName();


    private ListaPull.Fragment.View mView;
    private GitHubInteractor interactor;

    @Inject
    public ListaPullFragmentPresenterImpl(ListaPull.Fragment.View mView, GitHubInteractor interactor) {
        this.mView = mView;
        this.interactor = interactor;
    }

    @Override
    public void carregar(String usuario, String repositorio) {

        final CompositeSubscription subscription = new CompositeSubscription();

        mView.showProgress(true);
        subscription.add(interactor.obterPulls(usuario, repositorio)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<List<Pull>>() {
                    @Override
                    public void onCompleted() {
                        mView.showProgress(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, e.getMessage(), e);
                        mView.showErro(e.getMessage());
                        mView.showProgress(false);
                    }

                    @Override
                    public void onNext(List<Pull> pulls) {
                        if (pulls.size() == 0 ){
                            mView.showListaVazia(true);
                        } else {
                            mView.showListaVazia(false);
                            mView.addItem(pulls);
                        }
                    }
                }));

    }
}
