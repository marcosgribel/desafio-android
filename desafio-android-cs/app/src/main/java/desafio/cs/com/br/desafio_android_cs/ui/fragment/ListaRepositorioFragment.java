package desafio.cs.com.br.desafio_android_cs.ui.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import desafio.cs.com.br.desafio_android_cs.R;
import desafio.cs.com.br.desafio_android_cs.configuration.App;
import desafio.cs.com.br.desafio_android_cs.configuration.di.module.ListaRepositorioFragmentModule;
import desafio.cs.com.br.desafio_android_cs.model.entity.Item;
import desafio.cs.com.br.desafio_android_cs.presentation.adapter.RepositorioArrayAdapter;
import desafio.cs.com.br.desafio_android_cs.presentation.presenter.ListaRepositorioFragmentPresenter;
import desafio.cs.com.br.desafio_android_cs.presentation.view.BaseView;
import desafio.cs.com.br.desafio_android_cs.presentation.view.ListaRepositorio;
import desafio.cs.com.br.desafio_android_cs.ui.OnRecyclerItemClickListener;
import desafio.cs.com.br.desafio_android_cs.ui.activity.ListaPullActivity;
import desafio.cs.com.br.desafio_android_cs.presentation.adapter.RepositoryRecyclerAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListaRepositorioFragment extends BaseFragment implements ListaRepositorio.Fragment.View {

    private static final String TAG = ListaRepositorioFragment.class.getSimpleName();

    @Inject
    ListaRepositorioFragmentPresenter presenter;


    @BindView(R.id.list_view_repositories)
    ListView mListView;

    RepositorioArrayAdapter mAdapter;
    private int mCurrentPage = 1;



    private final ListView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Item item = mAdapter.getItem(position);

            Intent intent = new Intent(getContext(), ListaPullActivity.class);
            intent.putExtra(getString(R.string.nome_usuario), item.getOwner().getLogin());
            intent.putExtra(getString(R.string.nome_repositorio), item.getName());
            intent.putExtra(getString(R.string.qtd_pull), item.getOpenIssuesCount());
            intent.putExtra(getString(R.string.qtd_pull_fechado), item.getOpenIssuesCount());

           startActivity(intent);

        }
    };

    private ListView.OnScrollListener mOnScrollChangeListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            int lastVisibleItem = firstVisibleItem + visibleItemCount;

            if( lastVisibleItem >= totalItemCount ){
                mCurrentPage++;
                presenter.carregar(mCurrentPage);
            }
        }
    };



    public ListaRepositorioFragment() {
        // Required empty public constructor
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(getString(R.string.itens), (ArrayList<? extends Parcelable>) mAdapter.getItens());
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Dagger dependency injection configuration
        App.get(this.getContext())
                .getAppComponent()
                .plus(new ListaRepositorioFragmentModule(this))
                .inject(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_lista_repositorio, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(savedInstanceState == null){
            mAdapter = new RepositorioArrayAdapter(getContext(), R.layout.item_list_repositories);
            presenter.carregar(mCurrentPage);
        }else{
            List<Item> itens = savedInstanceState.getParcelableArrayList(getString(R.string.itens));
            mAdapter = new RepositorioArrayAdapter(getContext(), R.layout.item_list_repositories, itens);
        }

        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(mOnItemClickListener);
        mListView.setOnScrollListener(mOnScrollChangeListener);

    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void addItem(List<Item> itens) {
        if( mAdapter != null){
            mAdapter.addAll(itens);
        }
    }


}
