package desafio.cs.com.br.desafio_android_cs.configuration.di.module;

import dagger.Module;
import dagger.Provides;
import desafio.cs.com.br.desafio_android_cs.configuration.PerActivity;
import desafio.cs.com.br.desafio_android_cs.presentation.presenter.ListaRepositorioFragmentPresenter;
import desafio.cs.com.br.desafio_android_cs.presentation.presenter.impl.ListaRepositorioFragmentPresenterImpl;
import desafio.cs.com.br.desafio_android_cs.presentation.view.ListaRepositorio;


/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */

@Module(
        includes = InteractorModule.class
)
public class ListaRepositorioFragmentModule {

    public final ListaRepositorio.Fragment.View mView;

    public ListaRepositorioFragmentModule(ListaRepositorio.Fragment.View mView) {
        this.mView = mView;
    }

    @PerActivity
    @Provides
    ListaRepositorio.Fragment.View provideListaRepositorioFragmentView(){
        return this.mView;
    }


    @PerActivity
    @Provides
    ListaRepositorioFragmentPresenter provideListaRepositorioFragment(ListaRepositorioFragmentPresenterImpl presenter){
        return presenter;
    }
}
