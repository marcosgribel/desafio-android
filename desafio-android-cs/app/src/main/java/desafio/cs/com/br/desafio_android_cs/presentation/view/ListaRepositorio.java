package desafio.cs.com.br.desafio_android_cs.presentation.view;

import java.util.List;

import desafio.cs.com.br.desafio_android_cs.model.entity.Item;

/**
 * Created by marcosgribel on 10/8/16.
 */

public interface ListaRepositorio {


    interface View extends BaseView {

    }


    interface Fragment {

        interface View extends ListaRepositorio.View {

            void addItem(List<Item> itens);
//
//            void showErro(String mensagem);
//
//            void showProgress(boolean isShow);
        }

    }



}
