package desafio.cs.com.br.desafio_android_cs.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import desafio.cs.com.br.desafio_android_cs.R;
import desafio.cs.com.br.desafio_android_cs.ui.fragment.ListaPullFragment;

public class ListaPullActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pull);


        if(savedInstanceState == null) {

            Bundle args = getIntent().getExtras();

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content,
                            ListaPullFragment.newInstance(
                                    args.getString(getString(R.string.nome_usuario)),
                                    args.getString(getString(R.string.nome_repositorio)),
                                    args.getInt(getString(R.string.qtd_pull)),
                                    args.getInt(getString(R.string.qtd_pull_fechado)))
                    )
                    .commit();
        }

    }
}
