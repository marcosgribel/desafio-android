package desafio.cs.com.br.desafio_android_cs.model.service;

import java.util.List;
import java.util.Map;

import desafio.cs.com.br.desafio_android_cs.model.entity.Pull;
import desafio.cs.com.br.desafio_android_cs.model.entity.SchemaRepository;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */

public interface GitHubEndPoint {


    @GET("/search/repositories?q=language:Java")
    Observable<SchemaRepository> repositories(@QueryMap Map<String, Object> query);

    @GET("/repos/{user}/{repository}/pulls")
    Observable<List<Pull>> pulls(@Path("user") String user, @Path("repository") String repository);

}
