package desafio.cs.com.br.desafio_android_cs.model.service;

import android.support.compat.BuildConfig;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */

public class RetrofitConnection {


    private static final OkHttpClient.Builder httpClientBluider = new OkHttpClient.Builder();
    private static final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
    private static final HttpLoggingInterceptor.Level loggingLevel = HttpLoggingInterceptor.Level.BODY;


    public RetrofitConnection() {


    }

    /**
     * Method to connect service by retrofit library
     * @param serviceClass - generic class service
     * @param <S> - generic class service
     * @return service connection
     */
    public static <S> S connect(Class<S> serviceClass) {

        loggingInterceptor.setLevel(loggingLevel);
        httpClientBluider.interceptors().add(loggingInterceptor);


        httpClientBluider.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("username", "gribel.marcos@gmail.com")
                        .method(original.method(), original.body());

                return chain.proceed(requestBuilder.build());
            }
        });


        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create());

        OkHttpClient httpClient = httpClientBluider
                .addInterceptor(loggingInterceptor)
                .build();

        Retrofit retrofit = builder.client(httpClient).build();
        return retrofit.create(serviceClass);
    }




}
