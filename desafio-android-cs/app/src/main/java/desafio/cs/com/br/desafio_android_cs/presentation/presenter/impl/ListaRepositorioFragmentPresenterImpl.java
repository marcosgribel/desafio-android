package desafio.cs.com.br.desafio_android_cs.presentation.presenter.impl;

import android.util.Log;

import javax.inject.Inject;

import desafio.cs.com.br.desafio_android_cs.business.interactor.GitHubInteractor;
import desafio.cs.com.br.desafio_android_cs.model.entity.SchemaRepository;
import desafio.cs.com.br.desafio_android_cs.presentation.presenter.ListaRepositorioFragmentPresenter;
import desafio.cs.com.br.desafio_android_cs.presentation.view.ListaRepositorio;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */

public class ListaRepositorioFragmentPresenterImpl implements ListaRepositorioFragmentPresenter {

    private static final String TAG = ListaRepositorioFragmentPresenterImpl.class.getSimpleName();

    private ListaRepositorio.Fragment.View mView;
    private GitHubInteractor interactor;

    @Inject
    public ListaRepositorioFragmentPresenterImpl(ListaRepositorio.Fragment.View mView, GitHubInteractor interactor) {
        this.mView = mView;
        this.interactor = interactor;
    }


    public void carregar(int pagina){
        final CompositeSubscription subscription = new CompositeSubscription();

        mView.showProgress(true);
        subscription.add(interactor.obterRepositorios("stars", pagina)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<SchemaRepository>() {
            @Override
            public void onCompleted() {
                mView.showProgress(false);
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, e.getMessage(), e);
                mView.showErro(e.getMessage());
                mView.showProgress(false);
            }

            @Override
            public void onNext(SchemaRepository schemaRepository) {
                mView.addItem(schemaRepository.getItems());
            }
        }));

    }
}
