package desafio.cs.com.br.desafio_android_cs.business.interactor.impl;

import android.util.Log;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import desafio.cs.com.br.desafio_android_cs.business.interactor.GitHubInteractor;
import desafio.cs.com.br.desafio_android_cs.model.entity.Pull;
import desafio.cs.com.br.desafio_android_cs.model.entity.SchemaRepository;
import desafio.cs.com.br.desafio_android_cs.model.service.GitHubEndPoint;
import desafio.cs.com.br.desafio_android_cs.model.service.RetrofitConnection;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */

public class GitHubInteractorImpl implements GitHubInteractor {

    private static final String TAG = GitHubInteractorImpl.class.getSimpleName();


    @Inject
    public GitHubInteractorImpl() {

    }

    @Override
    public Observable<SchemaRepository> obterRepositorios(String sort, int page) {
        Map<String, Object> query = new HashMap<>();
        query.put("sort", "stars");
        query.put("page", page);

        return RetrofitConnection.connect(GitHubEndPoint.class).repositories(query);
    }

    @Override
    public Observable<List<Pull>> obterPulls(String usuario, String repositorio) {
        return RetrofitConnection.connect(GitHubEndPoint.class).pulls(usuario, repositorio);
    }


}
