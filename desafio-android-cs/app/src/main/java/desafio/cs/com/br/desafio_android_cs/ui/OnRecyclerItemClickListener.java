package desafio.cs.com.br.desafio_android_cs.ui;

import android.view.View;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */

public interface OnRecyclerItemClickListener {

    public void onClick(View view, int position);


}
