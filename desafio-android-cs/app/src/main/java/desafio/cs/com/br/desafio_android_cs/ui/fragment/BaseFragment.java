package desafio.cs.com.br.desafio_android_cs.ui.fragment;

import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import butterknife.BindView;
import desafio.cs.com.br.desafio_android_cs.R;
import desafio.cs.com.br.desafio_android_cs.model.entity.Item;
import desafio.cs.com.br.desafio_android_cs.presentation.view.BaseView;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */

public class BaseFragment extends Fragment implements BaseView {

    @BindView(R.id.progress)
    ProgressBar progressBar;


    @Override
    public void showErro(String mensagem) {
        Snackbar.make(getView(), mensagem, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showProgress(boolean isShow) {
        if(isShow){
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

}
