package desafio.cs.com.br.desafio_android_cs.configuration.di.module;

import dagger.Module;
import dagger.Provides;
import desafio.cs.com.br.desafio_android_cs.business.interactor.GitHubInteractor;
import desafio.cs.com.br.desafio_android_cs.business.interactor.impl.GitHubInteractorImpl;
import desafio.cs.com.br.desafio_android_cs.configuration.PerActivity;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */

@Module
public class InteractorModule {


    @PerActivity
    @Provides
    public GitHubInteractor provideRepositoryInteractor(GitHubInteractorImpl interactor){
        return interactor;
    }

}
