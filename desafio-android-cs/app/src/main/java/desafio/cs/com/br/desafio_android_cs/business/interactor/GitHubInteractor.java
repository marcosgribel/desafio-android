package desafio.cs.com.br.desafio_android_cs.business.interactor;

import java.util.List;

import desafio.cs.com.br.desafio_android_cs.model.entity.Item;
import desafio.cs.com.br.desafio_android_cs.model.entity.Pull;
import desafio.cs.com.br.desafio_android_cs.model.entity.SchemaRepository;
import rx.Observable;

/**
 * Copyright © 2016 by Marcos Gribel (gribel.marcos@gmail.com)
 * <p>
 * All Rights Reserved.
 */

public interface GitHubInteractor {


    Observable<SchemaRepository> obterRepositorios(String sort, int page);

    Observable<List<Pull>> obterPulls(String usuario, String repositorio);

}
