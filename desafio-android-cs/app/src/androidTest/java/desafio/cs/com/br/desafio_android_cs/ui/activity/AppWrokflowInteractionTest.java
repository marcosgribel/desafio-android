package desafio.cs.com.br.desafio_android_cs.ui.activity;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import desafio.cs.com.br.desafio_android_cs.R;
import desafio.cs.com.br.desafio_android_cs.model.entity.Item;
import desafio.cs.com.br.desafio_android_cs.model.entity.Pull;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.instanceOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class AppWrokflowInteractionTest {

    @Rule
    public ActivityTestRule<ListaRepositorioActivity> mActivityTestRule = new ActivityTestRule<>(ListaRepositorioActivity.class);

    @Test
    public void appWrokflowInteractionTest() {

        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Open navigation drawer"),
                        withParent(withId(R.id.toolbar)),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        pressBack();

        //Test para verificar a paginação infinita
        int TO_POSITION = 100;

            onData(anything())
                    .inAdapterView(allOf(withId(R.id.list_view_repositories), isDisplayed()))
                    .atPosition(TO_POSITION);


        onData(anything())
                .inAdapterView(allOf(withId(R.id.list_view_repositories), isDisplayed()))
                .atPosition(TO_POSITION);

        onData(instanceOf(Item.class))
                .inAdapterView(allOf(withId(R.id.list_view_repositories), isDisplayed()))
                .atPosition(TO_POSITION)
                .check(matches(isDisplayed()))
                .perform(click());

        onView(allOf(withContentDescription("Navigate up"),
                        withParent(allOf(withId(R.id.action_bar),
                                withParent(withId(R.id.action_bar_container)))),
                        isDisplayed()))
                .perform(click());


        // ALTERA O VALOR PARA RETORNAR AO INICIO DA LISTA
        TO_POSITION = 2;

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onData(anything())
                .inAdapterView(allOf(withId(R.id.list_view_repositories), isDisplayed()))
                .atPosition(TO_POSITION);

        onData(instanceOf(Item.class))
                .inAdapterView(allOf(withId(R.id.list_view_repositories), isDisplayed()))
                .atPosition(TO_POSITION)
                .perform(click());

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        onData(instanceOf(Pull.class))
                .inAdapterView(allOf(withId(R.id.list_view_pulls), isDisplayed()))
                .atPosition(0)
                .check(matches(isDisplayed()))
                .perform(click());

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
